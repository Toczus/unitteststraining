import electricity.ExistElectricityMeterTest;
import electricity.TwoElectricityMetersTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Adam Toczek on 12.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */

@RunWith(Suite.class)

@Suite.SuiteClasses({
        ExistElectricityMeterTest.class,
        ExistElectricityMeterTest.class,
        TwoElectricityMetersTest.class
})
public class ElectricityMeterSuite {
}
