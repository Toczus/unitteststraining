package electricity;

import java.util.Calendar;

/**
 * Created by Adam Toczek on 12.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */

// Licznik zużycia energii elektrycznej - odpowiednik tego co mamy na korytarzu
// electricity consumption meter - the equivalent of what we have in the corridor
class ElectricityMeter {

    TariffProvider tp;

    private float kwh = 0;                      // zlicza kilowatogodziny / counts kilowatts
    private int centsForKwh = 0;                // Przelicza na centy / // Converts to cents

    private boolean tariffOn = false;          // Funkcja włączenia taryfy kiedy prąd jest tańszy / Function of switching on the tariff when electricity is cheaper
    private float kwhTariff = 0;
    private int centsForKwhTariff = 0;

    private int electricityTariffStartHour = 0;
    private int electricityTariffEndHour = 0;

    public ElectricityMeter() {
        tp = new TariffProvider() {
            @Override
            public boolean isTariffNow() {
                Calendar rightNow = Calendar.getInstance();
                int hour = rightNow.get(Calendar.HOUR_OF_DAY);
                return hour > electricityTariffStartHour && hour < electricityTariffEndHour;
            }
        };
    }

    public ElectricityMeter(TariffProvider tp) {
        this.tp = tp;
    }

    // Metoda na podstawie taryfy dodaje zużycie do odpowiedniego licznika
    // The tariff-based method adds consumption to the appropriate meter
    public void addKwh(float kwhToAdd) {
        if (isTariffNow()) {
            kwhTariff += kwhToAdd;
        } else {
            kwh += kwhToAdd;
        }
    }

    // Metoda sprawdzająca czy w danym momencie jest włączona taryfa
    // The method of checking whether the tariff is activated at the moment
    public boolean isTariffNow() {
        return tp.isTariffNow();
    }

    /**
     * @return how much more expensive is normal price comparing to tariff in percentages
     */
    // Metoda określająca ile procent prąd jest droższy poza taryfą
    public int getHowMoreExpensiveNormalIs() {
        return (centsForKwh * 100 / centsForKwhTariff) - 100;
    }

    public float getKwh() {
        return kwh;
    }

    public float getKwhTariff() {
        return kwhTariff;
    }

    void setCentsForKwh(int centsForKwh) {
        this.centsForKwh = centsForKwh;
    }

    void setTariffOn(boolean tariffOn) {
        this.tariffOn = tariffOn;
    }

    void setCentsForKwhTariff(int centsForKwhTariff) {
        this.centsForKwhTariff = centsForKwhTariff;
    }

    void setElectricityTariffStartHour(int electricityTariffStartHour) {
        this.electricityTariffStartHour = electricityTariffStartHour;
    }

    void setElectricityTariffEndHour(int electricityTariffEndHour) {
        this.electricityTariffEndHour = electricityTariffEndHour;
    }

    public void reset() {
        this.kwh = 0;
        this.centsForKwh = 0;
        this.tariffOn = false;
        this.kwhTariff = 0;
        this.centsForKwhTariff = 0;
        this.electricityTariffStartHour = 0;
        this.electricityTariffEndHour = 0;
    }
}
