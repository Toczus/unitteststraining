package electricity;

/**
 * Created by Adam Toczek on 14.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public interface TariffProvider {
    boolean isTariffNow();
}
